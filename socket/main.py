from flask import Flask, request, render_template, Response, send_from_directory
from flask_socketio import SocketIO, send, emit
import os
import random

app = Flask(__name__)
app.config['SECRET_KEY'] = os.environ.get('FLASK_SECRET')

socketio = SocketIO(app,cors_allowed_origins="*")

names = ['job','naii','im','sasi','duan','mil','M','ming','first','nita','angie','mime']
current = ""
@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('js', path)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/admin')
def admin():
    return render_template('admin.html')

@socketio.on('command')
def update(data):
    global current
    r = random.choice(names)
    data = {'n1':current, 'n2':r}
    emit('command',data, broadcast=True)
    current = r